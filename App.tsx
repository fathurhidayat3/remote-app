import React from 'react';
import {ApplicationProvider, IconRegistry} from '@ui-kitten/components';
import {EvaIconsPack} from '@ui-kitten/eva-icons';
import {mapping, light} from '@eva-design/eva';
import {FirebaseAuthTypes} from '@react-native-firebase/auth';

import Login from './src/screens/Login';
import Home from './src/screens/Home';
import ThemeContext from './src/contexts/ThemeContext';
import AuthContext from './src/contexts/AuthContext';

const App = (): React.ReactFragment => {
  const [user, setUser] = React.useState<
    FirebaseAuthTypes.UserCredential | undefined
  >(undefined);
  const [theme, setTheme] = React.useState(light);

  const handleSetUserData = (user: FirebaseAuthTypes.UserCredential) => {
    setUser(user);
  };

  return (
    <>
      <IconRegistry icons={EvaIconsPack} />
      <ApplicationProvider mapping={mapping} theme={theme}>
        <ThemeContext.Provider
          value={{currentTheme: theme, toggleTheme: setTheme}}>
          <AuthContext.Provider
            value={{userData: user, setUserData: handleSetUserData}}>
            <AuthContext.Consumer>
              {({userData}) =>
                userData && !!userData.user ? <Home /> : <Login />
              }
            </AuthContext.Consumer>
          </AuthContext.Provider>
        </ThemeContext.Provider>
      </ApplicationProvider>
    </>
  );
};

export default App;
