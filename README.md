# Instalasi dan run di development mode

1. Install node.js (https://www.npmjs.com/package/node)
2. Install yarn (https://www.npmjs.com/package/yarn)
3. Install dan atur konfigurasi React Native dengan CLI (https://facebook.github.io/react-native/docs/getting-started)
4. Masuk ke folder project dengan terminal/command prompt
5. Jalankan perintah `yarn`
6. Pastikan sudah ada emulator untuk android / ios yang siap dipakai
7. Jika ingin menjalakan di device/hp secara langsung ikuti langkah di https://facebook.github.io/react-native/docs/getting-started pada bagian `Using a physical device` untuk android atau `Running on a device` untuk ios
8. Untuk run di android jalankan `react-native run-android`
9. Sedangkan untuk run di ios jalankan `react-native run-ios`
10. Akan muncul jendela metro bundler untuk mengompile file project, tunggu beberapa saat
11. Ubah kode yang ada maka hot reload akan berjalan dan menampilkan perubahan
