import React from 'react';
import {Layout, Toggle, Card, CardHeader} from '@ui-kitten/components';
import database from '@react-native-firebase/database';

const LampInstanceComp = (props: {title: string; dbRef: string}) => {
  const {title, dbRef} = props;
  const [lamp, setLamp] = React.useState(false);

  const refLamp = database().ref(dbRef);

  React.useEffect(() => {
    refLamp.on('value', getLamp);

    return () => {
      refLamp.off('value', getLamp);
    };
  });

  const getLamp = (snapshot: any) => {
    setLamp(snapshot.val());
  };

  const handleLampChange = () => {
    refLamp.set(!lamp);
  };

  return (
    <Layout style={{marginTop: 16, paddingHorizontal: 8, width: '50%'}}>
      <Card
        header={() => (
          <CardHeader
            title={title}
            description={`Status: ${lamp ? 'on' : 'off'}`}
          />
        )}
        status={lamp ? 'success' : 'danger'}>
        <Toggle
          checked={lamp}
          status={lamp ? 'success' : 'danger'}
          onChange={handleLampChange}
        />
      </Card>
    </Layout>
  );
};

export default LampInstanceComp;
