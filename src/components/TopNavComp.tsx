import React from 'react';
import {StyleProp} from 'react-native';
import {Icon, TopNavigation, TopNavigationAction} from '@ui-kitten/components';
import {light, dark} from '@eva-design/eva';
import auth from '@react-native-firebase/auth';

import ThemeContext from '../contexts/ThemeContext';
import AuthContext from '../contexts/AuthContext';

const LightIcon = (style: StyleProp<any>) => (
  <Icon {...style} name="sun-outline" />
);
const DarkIcon = (style: StyleProp<any>) => (
  <Icon {...style} name="moon-outline" />
);
const SignOutIcon = (style: StyleProp<any>) => (
  <Icon {...style} name="log-out-outline" />
);

const ThemeToggleAction = (props: any) => {
  const {currentTheme} = React.useContext(ThemeContext);

  return (
    <TopNavigationAction
      {...props}
      icon={currentTheme === dark ? LightIcon : DarkIcon}
    />
  );
};

const SignOutAction = (props: any) => (
  <TopNavigationAction {...props} icon={SignOutIcon} />
);

const TopNavComp = () => {
  const {currentTheme, toggleTheme} = React.useContext(ThemeContext);
  const {userData, setUserData} = React.useContext(AuthContext);

  const handleToggleTheme = () => {
    currentTheme === dark ? toggleTheme(light) : toggleTheme(dark);
  };

  const handleSignOut = () => {
    auth().signOut();
    setUserData({});
  };

  const renderRightControls = () => [
    <ThemeToggleAction onPress={handleToggleTheme} />,
    <SignOutAction onPress={handleSignOut} />,
  ];

  return (
    <TopNavigation
      title={`Hello ${userData && userData.user && userData.user.email}`}
      rightControls={renderRightControls()}
    />
  );
};

export default TopNavComp;
