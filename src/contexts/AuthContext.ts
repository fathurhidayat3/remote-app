import React from 'react';
import {FirebaseAuthTypes} from '@react-native-firebase/auth';

const AuthContext = React.createContext<any>({
  userData: {},
  setUserData: (user: FirebaseAuthTypes.UserCredential) => {},
});

export default AuthContext;
