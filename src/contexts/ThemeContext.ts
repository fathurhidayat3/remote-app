import React from 'react';
import {light} from '@eva-design/eva';

const ThemeContext = React.createContext<any>({
  currentTheme: light,
  toggleTheme: () => {},
});

export default ThemeContext;
