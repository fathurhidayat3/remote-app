import React from 'react';
import {FlatList, Platform} from 'react-native';
import {Layout, Select, Text} from '@ui-kitten/components';
import database from '@react-native-firebase/database';

import TopNavComp from '../components/TopNavComp';
import LampInstanceComp from '../components/LampInstanceComp';

const Home = () => {
  const [selectedOption, setSelectedOption]: any = React.useState({
    text: 'Room 1',
    value: 'room1',
  });
  const [lamps, setLamps] = React.useState([]);

  const data = [
    {text: 'Room 1', value: 'room1'},
    {text: 'Room 2', value: 'room2'},
    {text: 'Room 3', value: 'room3'},
  ];

  const onSnapshot = (snapshot: any) => {
    const listLamp: any = [];

    snapshot.forEach((lampItem: any) => {
      listLamp.push(lampItem);
    });

    setLamps(listLamp);
  };

  React.useEffect(() => {
    const ref = database().ref(selectedOption.value);
    ref.once('value', onSnapshot);
  });

  return (
    <>
      <Layout
        style={{
          flex: 1,

          paddingTop: Platform.OS === 'ios' ? 16 : 0,
          paddingHorizontal: 8,
          paddingBottom: 8,
        }}>
        <TopNavComp />
        <Layout style={{marginTop: 8, paddingHorizontal: 4, width: '100%'}}>
          <Select
            data={data}
            selectedOption={selectedOption}
            onSelect={setSelectedOption}
          />
        </Layout>

        <FlatList
          data={lamps}
          renderItem={({index}) => (
            <LampInstanceComp
              title={`Lampu ${index}`}
              dbRef={`${selectedOption.value}/lamp${index}`}
              key={index}
            />
          )}
          // keyExtractor={({index}) => index}
          numColumns={2}
        />
      </Layout>
    </>
  );
};

export default Home;
