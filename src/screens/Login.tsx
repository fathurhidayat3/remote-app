import React from 'react';
import {StyleProp} from 'react-native';
import {Layout, Button, Icon, Input, Text} from '@ui-kitten/components';
import auth from '@react-native-firebase/auth';

import AuthContext from '../contexts/AuthContext';

const Login = () => {
  const {setUserData} = React.useContext(AuthContext);
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [secureTextEntry, setSecureTextEntry] = React.useState(true);
  const [inputLoginStatus, setInputLoginStatus] = React.useState('basic');

  const handleOnIconPress = () => {
    setSecureTextEntry(!secureTextEntry);
  };

  const renderIcon = (style: StyleProp<any>) => (
    <Icon {...style} name={secureTextEntry ? 'eye-off' : 'eye'} />
  );

  const handleSignIn = async () => {
    await auth()
      .signInWithEmailAndPassword(email, password)
      .then(res => {
        setUserData(res);
      })
      .catch(() => setInputLoginStatus('danger'));
  };

  return (
    <Layout
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',

        paddingTop: 36,
        paddingHorizontal: 8,
        paddingBottom: 8,
      }}>
      <Layout>
        <Text
          style={{
            textAlign: 'center',
          }}
          category="h1">
          Remote App
        </Text>
        <Text
          style={{
            textAlign: 'center',
          }}
          appearance="hint">
          Control your devices on the cloud
        </Text>
      </Layout>
      <Layout
        style={{
          paddingTop: 36,
          paddingHorizontal: 8,
          paddingBottom: 8,

          width: '100%',
        }}>
        <Layout>
          <Input
            value={email}
            status={inputLoginStatus}
            placeholder="Fill in your email"
            onChangeText={setEmail}
          />
        </Layout>
        <Layout style={{marginTop: 8}}>
          <Input
            value={password}
            status={inputLoginStatus}
            placeholder="Your password's here"
            icon={renderIcon}
            secureTextEntry={secureTextEntry}
            onIconPress={handleOnIconPress}
            onChangeText={setPassword}
          />
        </Layout>
        <Layout style={{marginTop: 8}}>
          <Button onPress={handleSignIn}>Sign In</Button>
        </Layout>
      </Layout>
    </Layout>
  );
};

export default Login;
